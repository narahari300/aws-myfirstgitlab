Course Notes :

narahari@Mahesh MINGW64 ~
$ cd aws-myfirstgitlab/day1/

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ pwd
/c/Users/narahari/aws-myfirstgitlab/day1

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ touch runningnotes.txt

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ ls -l
total 0
-rw-r--r-- 1 narahari 197609 0 Aug 29 10:14 runningnotes.txt

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ pwd
/c/Users/narahari/aws-myfirstgitlab/day1

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ ls
day1.txt  runningnotes.txt

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ cat runningnotes.txt

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ cat runningnotes.txt
Course Notes :

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ pwd
/c/Users/narahari/aws-myfirstgitlab/day1

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ cd ..

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        deleted:    README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        aws-myfirstgitlab/
        day1/

no changes added to commit (use "git add" and/or "git commit -a")

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ git add
Nothing specified, nothing added.
hint: Maybe you wanted to say 'git add .'?
hint: Turn this message off by running
hint: "git config advice.addEmptyPathspec false"

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ git config advice.addEmptyPathspec false

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab/day1 (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        deleted:    ../README.md
        new file:   day1.txt
        new file:   runningnotes.txt

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
$ git commit -m "Day1 running notes"

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: unable to auto-detect email address (got 'narahari@Mahesh.(none)')

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
$ git config --global user.email "narahari428@gmail.com"

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
$  git config --global user.name "vharix"
narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        deleted:    README.md
        new file:   day1/day1.txt
        new file:   day1/runningnotes.txt
narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
$ git commit -m "Day1 notes"
[master 8a44eab] Day1 notes
 3 files changed, 100 insertions(+), 3 deletions(-)
 delete mode 100644 README.md
 create mode 100644 day1/day1.txt
 create mode 100644 day1/runningnotes.txt

narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
narahari@Mahesh MINGW64 ~/aws-myfirstgitlab (master)
$ git push -u origin master
warning: redirecting to https://gitlab.com/narahari300/aws-myfirstgitlab.git/
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 4 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (5/5), 1.08 KiB | 370.00 KiB/s, done.
Total 5 (delta 0), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/narahari300/aws-myfirstgitlab
   cd538ce..8a44eab  master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.

